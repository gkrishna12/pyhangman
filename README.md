# README #

### What is this repository for? ###

* A simple game of Hangman in Python

### How do I get set up? ###

* Download the files in a folder.
* make sure both the files are in the same folder or the console may throw an error on startup.
* Run pyHangman.py and the game will start on the python console.

### Who do I talk to? ###

* K Gopal Krishna - mail@kgopalkrishna.com