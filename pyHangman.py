import random
import string

WORDLIST_FILENAME = "words.txt"

def loadWords():
    print("Loading word list from file...")
    inFile = open(WORDLIST_FILENAME, 'r')
    line = inFile.readline()
    wordlist = line.split()
    print("  ", len(wordlist), "words loaded.")
    return wordlist

def chooseWord(wordlist):
    return random.choice(wordlist)

wordlist = loadWords()

def isWordGuessed(secretWord, lettersGuessed):
    for letter in secretWord :
        if not letter in lettersGuessed :
            return False
    return True

def getGuessedWord(secretWord, lettersGuessed):
    word = ''
    for letter in secretWord :
        if not letter in lettersGuessed :
            word += '_ '
        else :
            word += letter
    return word


def getAvailableLetters(lettersGuessed):
    all_letters = string.ascii_lowercase
    available_letters = ''
    for letter in all_letters :
        if not letter in lettersGuessed :
            available_letters += letter
    return available_letters

def hangman(secretWord):
    guessesRemaining = 8
    lettersGuessed   = []
    guessedWord = ''
    
    print("Welcome to the game, Hangman!")
    print("I am thinking of a word that is " + str(len(secretWord)) + " letters long")
    print("-------------")
    while guessesRemaining != 0 and not isWordGuessed(secretWord, lettersGuessed):
        print("You have " + str(guessesRemaining) +" guesses left")
        print("Available letters: " + getAvailableLetters(lettersGuessed))
        guess = input("Please guess a letter: ")
        guessLower = guess.lower()
        if not guessLower in lettersGuessed :
            lettersGuessed.append(guessLower)
            guessedWord = getGuessedWord(secretWord,lettersGuessed)
            if guessLower in guessedWord :
                print("Good guess: " + guessedWord)
            else :
                print("Oops! That letter is not in my word: " + guessedWord)
                guessesRemaining -= 1
        else :
            print("Oops! You've already guessed that letter: " + guessedWord)
        print("------------")
        
    if isWordGuessed(secretWord, lettersGuessed) :
        print("Congratulations, you won!")
    if guessesRemaining == 0 :
        print("Sorry, you ran out of guesses. The word was " + secretWord)


secretWord = chooseWord(wordlist)
hangman(secretWord)
